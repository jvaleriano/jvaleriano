## Hello  

My name is Joseph Valeriano, and I am an [ENTJ-A](https://www.16personalities.com/entj-personality) living near Denver, Colorado. My wife and I have four children and a Bernedoodle. 

I joined GitLab in late-2018 after a career as an intelligence analyst in the Army -> systems engineer -> project lead -> principal consultant. My strengths are in strategic planning, business research and cross-functional project leadership.

### How I work

I love async work and am most effective in short bursts that allow me to provide value across a wide variety of areas. 

I leverage GitLab to organize and deliver the vast majority of my work, so my various activities are purposeful and transparent to my stakeholders. I am also active in Slack and have learned the value of the "remind me" feature to save myself from immediately answering every message with a Pavlovian-like response. 

### Meetings 

**I love 1:1 meetings** and am energized by learning more about people. I love learning about others' backgrounds, their motivations, and finding common ground with people.

I prefer time-constrained meetings to serve as a forcing function to reduce meeting "noise." I promise to respect your time by ensuring my meetings follow an agreed-upon agenda and by staying within time boundaries.

### Knowledge of GitLab 

_Fun for me is to take what I learn and teach others in a way that is accessible and applicable in their own lives._ — Daphne Oz

I'm certified in DevSecOps and Agile/other project management disciplines. I love helping people know more about these areas and typically learn something new along the way. 

### Social

I am not very active on social media, but I am known to reshare a GitLab post from time-to-time. If interested, you can follow me at:

- [LinkedIn](https://www.linkedin.com/in/jlvaleriano/)
